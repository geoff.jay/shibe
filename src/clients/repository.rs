use diesel;
use diesel::prelude::*;

use schema::{clients, projects};
use clients::model::{Client, NewClient, UpdateClient};
use projects::model::Project;

impl Client {
    pub fn all(conn: &MysqlConnection) -> QueryResult<Vec<Client>> {
        clients::table
            .order(clients::id)
            .load::<Client>(&*conn)
    }

    pub fn get(id: i32, conn: &MysqlConnection) -> QueryResult<Client> {
        clients::table.find(id).get_result::<Client>(conn)
    }

    pub fn get_projects(id: i32, conn: &MysqlConnection) -> QueryResult<Vec<Project>> {
        projects::dsl::projects
            .filter(projects::dsl::client_id.eq(id))
            .load::<Project>(&*conn)
    }

    // XXX: Might be able to change execute to get_result - http://diesel.rs/guides/all-about-inserts/
    pub fn insert(client: Client, conn: &MysqlConnection) -> QueryResult<Client> {
        diesel::insert_into(clients::table)
            .values(&NewClient::from_client(client))
            .execute(conn)
            .expect("insert client");

        clients::dsl::clients.order(clients::dsl::id.desc()).first(conn)
    }

    pub fn update(id: i32, client: UpdateClient, conn: &MysqlConnection) -> QueryResult<Client> {
        let mut client_in_db = Client::get(id, conn).unwrap();

        if let Some(code) = client.code.clone() {
            client_in_db.code = code;
        }

        diesel::update(clients::table.find(id))
            .set(&client_in_db)
            .execute(conn)
            .expect("update client");

        clients::table.find(id).get_result::<Client>(conn)
    }

    pub fn delete(id: i32, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::delete(clients::table.find(id))
            .execute(conn)
    }
}
