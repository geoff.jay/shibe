use rocket::Rocket;
use clients;

pub fn deploy(rocket: Rocket, mountpoint: &str) -> Result<Rocket, Rocket> {
    let rocket = rocket.mount(mountpoint,
                              routes![clients::handler::all,
                                      clients::handler::get,
                                      clients::handler::get_projects,
                                      clients::handler::post,
                                      clients::handler::put,
                                      clients::handler::delete]);
    Ok(rocket)
}
