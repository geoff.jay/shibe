use rocket::response::{Failure, status};
use rocket_contrib::Json;

use clients::model::{Client, UpdateClient};
use projects::model::Project;
use common::{error_status, host, port};
use database::DbConn;

#[get("/", format = "application/json")]
pub fn all(conn: DbConn) -> Result<Json<Vec<Client>>, Failure> {
    Client::all(&conn)
        .map(|client| Json(client))
        .map_err(|error| error_status(error))
}

#[get("/<id>", format = "application/json")]
fn get(id: i32, conn: DbConn) -> Result<Json<Client>, Failure> {
    Client::get(id, &conn)
        .map(|client| Json(client))
        .map_err(|error| error_status(error))
}

#[get("/<id>/projects", format = "application/json")]
fn get_projects(id: i32, conn: DbConn) -> Result<Json<Vec<Project>>, Failure> {
    Client::get_projects(id, &conn)
        .map(|client| Json(client))
        .map_err(|error| error_status(error))
}

#[post("/", format = "application/json", data = "<client>")]
fn post(client: Json<Client>, conn: DbConn) -> Result<status::Created<Json<Client>>, Failure> {
    let insert = Client { id: None, ..client.into_inner() };
    Client::insert(insert, &conn)
        .map(|client| client_created(client))
        .map_err(|error| error_status(error))
}

fn client_created(client: Client) -> status::Created<Json<Client>> {
    status::Created(
        format!("{host}:{port}/clients/{id}",
                host = host(),
                port = port(),
                id = client.id.unwrap())
            .to_string(),
        Some(Json(client)))
}

#[put("/<id>", format = "application/json", data = "<client>")]
fn put(id: i32, client: Json<UpdateClient>, conn: DbConn) -> Result<Json<Client>, Failure> {
    Client::update(id, client.into_inner(), &conn)
        .map(|rec| Json(rec))
        .map_err(|error| error_status(error))
}

#[delete("/<id>")]
fn delete(id: i32, conn: DbConn) -> Result<status::NoContent, Failure> {
    match Client::get(id, &conn) {
        Ok(_) => Client::delete(id, &conn)
            .map(|_| status::NoContent)
            .map_err(|error| error_status(error)),
        Err(error) => Err(error_status(error))
    }
}
