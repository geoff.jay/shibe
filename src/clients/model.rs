use schema::clients;

#[derive(Clone, Debug, Identifiable, Queryable, AsChangeset, Serialize, Deserialize)]
#[table_name = "clients"]
pub struct Client {
    pub id: Option<i32>,
    pub code: String,
}

#[derive(Debug, Insertable, Deserialize)]
#[table_name = "clients"]
pub struct NewClient {
    pub code: String,
}

#[derive(Debug, Deserialize)]
pub struct UpdateClient {
    pub code: Option<String>,
}

impl NewClient {
    pub fn from_client(client: Client) -> NewClient {
        NewClient {
            code: client.code,
        }
    }
}
