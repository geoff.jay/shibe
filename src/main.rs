//! Coanda Intranet Service
//!
//! This service is a database interface for Coanda data, it is meant to provide
//! access to corporate data displayed in the form of an intranet.

#![feature(plugin, decl_macro, custom_derive)]
#![plugin(rocket_codegen)]

//#[macro_use] extern crate log;
#[macro_use] extern crate clap;
#[macro_use] extern crate diesel;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

//extern crate env_logger;
extern crate dotenv;
extern crate rocket;

mod clients;
mod employees;
mod locations;
mod projects;
mod common;
mod schema;
mod database;

use clap::{App, ArgMatches, Shell};
use dotenv::dotenv;
use std::io::stdout;
use std::process;
use std::error::Error;
use rocket::fairing::AdHoc;
//use rocket::config::Environment;
use rocket_contrib::Json;

fn main() {
    //env_logger::init();
    dotenv().ok();

    match run() {
        Ok(_) => {}
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    }
}

fn run() -> Result<(), Box<Error>> {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();
    // TODO: Figure out how to use this instead of RUST_LOG, and remove _
    let _verbose = matches.is_present("verbose");

    match matches.subcommand() {
        ("start", Some(m)) => start(m)?,
        ("completions", Some(c)) => {
            if let Some(shell) = c.value_of("shell") {
                App::from_yaml(yaml).gen_completions_to(
                    "rmt",
                    shell.parse::<Shell>().unwrap(),
                    &mut stdout(),
                );
            }
        }
        (_, _) => unreachable!(),
    }

    Ok(())
}

#[catch(404)]
fn not_found() -> Json {
    Json(json!({
        "status": "error",
        "reason": "Resource was not found"
    }))
}

#[get("/")]
pub fn index() -> &'static str {
    "Coanda Intranet"
}

fn start(_m: &ArgMatches) -> Result<(), Box<Error>> {
    // XXX: Consider getting the configuration differently than toml?
    //let env: Environment;
    //match m.value_of("env").unwrap_or("development") {
        //"development" => env = Environment::Development,
        //"staging" => env = Environment::Staging,
        //"production" => env = Environment::Production,
    //}

    //::std::env::set_var("ROCKET_ENV", env.to_string());

    rocket::ignite()
        .manage(database::init_pool())
        .mount("", routes![index])
        .attach(AdHoc::on_attach(|rocket| { clients::router::deploy(rocket, "/v1/clients") }))
        .attach(AdHoc::on_attach(|rocket| { employees::router::deploy(rocket, "/v1/employees") }))
        .attach(AdHoc::on_attach(|rocket| { locations::router::deploy(rocket, "/v1/locations") }))
        .attach(AdHoc::on_attach(|rocket| { projects::router::deploy(rocket, "/v1/projects") }))
        .catch(catchers![not_found])
        .launch();

    Ok(())
}
