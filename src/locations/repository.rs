use diesel;
use diesel::prelude::*;

use schema::{employees, locations};
use locations::model::Location;
use employees::model::Employee;

impl Location {
    pub fn all(conn: &MysqlConnection) -> QueryResult<Vec<Location>> {
        locations::table.load::<Location>(&*conn)
    }

    pub fn get(id: i32, conn: &MysqlConnection) -> QueryResult<Location> {
        locations::table.find(id).get_result::<Location>(conn)
    }

    pub fn get_employees(id: i32, conn: &MysqlConnection) -> QueryResult<Vec<Employee>> {
        employees::dsl::employees
            .filter(employees::dsl::location_id.eq(id))
            .load::<Employee>(&*conn)
    }

    pub fn delete(id: i32, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::delete(locations::table.find(id))
            .execute(conn)
    }
}
