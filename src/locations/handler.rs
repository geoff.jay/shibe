use rocket::response::{Failure, status};
use rocket_contrib::Json;

use locations::model::Location;
use employees::model::Employee;
use common::error_status;
use database::DbConn;

#[get("/", format = "application/json")]
pub fn all(conn: DbConn) -> Result<Json<Vec<Location>>, Failure> {
    Location::all(&conn)
        .map(|location| Json(location))
        .map_err(|error| error_status(error))
}

#[get("/<id>", format = "application/json")]
fn get(id: i32, conn: DbConn) -> Result<Json<Location>, Failure> {
    Location::get(id, &conn)
        .map(|location| Json(location))
        .map_err(|error| error_status(error))
}

#[get("/<id>/employees", format = "application/json")]
fn get_employees(id: i32, conn: DbConn) -> Result<Json<Vec<Employee>>, Failure> {
    Location::get_employees(id, &conn)
        .map(|employee| Json(employee))
        .map_err(|error| error_status(error))
}

#[delete("/<id>")]
fn delete(id: i32, conn: DbConn) -> Result<status::NoContent, Failure> {
    match Location::get(id, &conn) {
        Ok(_) => Location::delete(id, &conn)
            .map(|_| status::NoContent)
            .map_err(|error| error_status(error)),
        Err(error) => Err(error_status(error))
    }
}
