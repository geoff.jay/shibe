use schema::locations;

#[derive(Debug, Identifiable, Queryable, Serialize)]
#[table_name = "locations"]
pub struct Location {
    pub id: Option<i32>,
    pub name: String,
    pub city: Option<String>,
}
