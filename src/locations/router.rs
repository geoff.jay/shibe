use rocket::Rocket;
use locations;

pub fn deploy(rocket: Rocket, mountpoint: &str) -> Result<Rocket, Rocket> {
    let rocket = rocket.mount(mountpoint,
                              routes![locations::handler::all,
                                      locations::handler::get,
                                      locations::handler::get_employees,
                                      locations::handler::delete]);
    Ok(rocket)
}
