use schema::projects;
use clients::model::Client;

#[derive(Clone, Debug, Identifiable, Queryable, Associations, AsChangeset, Serialize, Deserialize)]
#[belongs_to(Client)]
#[table_name = "projects"]
pub struct Project {
    pub id: Option<i32>,
    pub client_id: i32,
    pub code: String,
}

#[derive(Debug, Insertable, Deserialize)]
#[table_name = "projects"]
pub struct NewProject {
    pub client_id: i32,
    pub code: String,
}

#[derive(Debug, Deserialize)]
pub struct UpdateProject {
    pub client_id: Option<i32>,
    pub code: Option<String>,
}

impl NewProject {
    pub fn from_project(project: Project) -> NewProject {
        NewProject {
            code: project.code,
            client_id: project.client_id,
        }
    }
}
