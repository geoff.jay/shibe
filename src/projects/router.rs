use rocket::Rocket;
use projects;

pub fn deploy(rocket: Rocket, mountpoint: &str) -> Result<Rocket, Rocket> {
    let rocket = rocket.mount(mountpoint,
                              routes![projects::handler::all,
                                      projects::handler::get,
                                      projects::handler::post,
                                      projects::handler::put,
                                      projects::handler::delete]);
    Ok(rocket)
}
