use diesel;
use diesel::prelude::*;

use schema::projects;
use projects::model::{Project, NewProject, UpdateProject};

impl Project {
    pub fn all(conn: &MysqlConnection) -> QueryResult<Vec<Project>> {
        projects::table
            .order(projects::id)
            .load::<Project>(&*conn)
    }

    pub fn get(id: i32, conn: &MysqlConnection) -> QueryResult<Project> {
        projects::table
            .find(id)
            .get_result::<Project>(conn)
    }

    // XXX: Might be able to change execute to get_result - http://diesel.rs/guides/all-about-inserts/
    pub fn insert(project: Project, conn: &MysqlConnection) -> QueryResult<Project> {
        diesel::insert_into(projects::table)
            .values(&NewProject::from_project(project))
            .execute(conn)
            .expect("insert project");

        projects::dsl::projects
            .order(projects::dsl::id.desc())
            .first(conn)
    }

    pub fn update(id: i32, project: UpdateProject, conn: &MysqlConnection) -> QueryResult<Project> {
        let mut project_in_db = Project::get(id, conn).unwrap();

        if let Some(code) = project.code.clone() {
            project_in_db.code = code;
        }

        if let Some(client_id) = project.client_id {
            project_in_db.client_id = client_id;
        }

        diesel::update(projects::table.find(id))
            .set(&project_in_db)
            .execute(conn)
            .expect("update project");

        projects::table.find(id).get_result::<Project>(conn)
    }

    pub fn delete(id: i32, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::delete(projects::table.find(id))
            .execute(conn)
    }
}
