use rocket::response::{Failure, status};
use rocket_contrib::Json;

use projects::model::{Project, UpdateProject};
use common::{error_status, host, port};
use database::DbConn;

#[get("/", format = "application/json")]
pub fn all(conn: DbConn) -> Result<Json<Vec<Project>>, Failure> {
    Project::all(&conn)
        .map(|project| Json(project))
        .map_err(|error| error_status(error))
}

#[get("/<id>", format = "application/json")]
fn get(id: i32, conn: DbConn) -> Result<Json<Project>, Failure> {
    Project::get(id, &conn)
        .map(|project| Json(project))
        .map_err(|error| error_status(error))
}

#[post("/", format = "application/json", data = "<project>")]
fn post(project: Json<Project>, conn: DbConn) -> Result<status::Created<Json<Project>>, Failure> {
    let insert = Project { id: None, ..project.into_inner() };
    Project::insert(insert, &conn)
        .map(|project| project_created(project))
        .map_err(|error| error_status(error))
}

fn project_created(project: Project) -> status::Created<Json<Project>> {
    status::Created(
        format!("{host}:{port}/projects/{id}",
                host = host(),
                port = port(),
                id = project.id.unwrap())
            .to_string(),
        Some(Json(project)))
}

#[put("/<id>", format = "application/json", data = "<project>")]
fn put(id: i32, project: Json<UpdateProject>, conn: DbConn) -> Result<Json<Project>, Failure> {
    Project::update(id, project.into_inner(), &conn)
        .map(|rec| Json(rec))
        .map_err(|error| error_status(error))
}

#[delete("/<id>")]
fn delete(id: i32, conn: DbConn) -> Result<status::NoContent, Failure> {
    match Project::get(id, &conn) {
        Ok(_) => Project::delete(id, &conn)
            .map(|_| status::NoContent)
            .map_err(|error| error_status(error)),
        Err(error) => Err(error_status(error))
    }
}
