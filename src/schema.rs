table! {
    clients (id) {
        id -> Nullable<Integer>,
        code -> Varchar,
    }
}

table! {
    employees (id) {
        id -> Nullable<Integer>,
        username -> Varchar,
        first_name -> Varchar,
        last_name -> Varchar,
        status_id -> Integer,
        location_id -> Integer,
    }
}

table! {
    employee_statuses (id) {
        id -> Nullable<Integer>,
        status -> Varchar,
    }
}

table! {
    locations (id) {
        id -> Nullable<Integer>,
        name -> Varchar,
        city -> Nullable<Varchar>,
    }
}

table! {
    projects (id) {
        id -> Nullable<Integer>,
        client_id -> Integer,
        code -> Varchar,
    }
}

joinable!(employees -> employee_statuses (status_id));
joinable!(employees -> locations (location_id));
joinable!(projects -> clients (client_id));

allow_tables_to_appear_in_same_query!(
    clients,
    employees,
    employee_statuses,
    locations,
    projects,
);
