use diesel::result::Error;
use rocket::http::Status;
use rocket::response::Failure;

pub fn host() -> String {
    //env::var("ROCKET_ADDRESS").expect("ROCKET_ADDRESS must be set")
    //config::Environment::active().unwrap().get_str("address").to_string()
    // FIXME: Get from config environment
    "localhost".to_string()
}

pub fn port() -> String {
    //env::var("ROCKET_PORT").expect("ROCKET_PORT must be set")
    //config::Environment::active().unwrap().get_str("port")
    // FIXME: Get from config environment
    "8000".to_string()
}

pub fn error_status(error: Error) -> Failure {
    Failure(match error {
        Error::NotFound => Status::NotFound,
        _ => Status::InternalServerError
    })
}
