use diesel;
use diesel::prelude::*;

use schema::employees;
use employees::model::{Employee, NewEmployee, UpdateEmployee};

impl Employee {
    /// Retrieve all employee records from the database
    pub fn all(conn: &MysqlConnection) -> QueryResult<Vec<Employee>> {
        employees::table.load::<Employee>(&*conn)
    }

    /// Retrieve a single employee record from the database
    pub fn get(id: i32, conn: &MysqlConnection) -> QueryResult<Employee> {
        employees::table.find(id).get_result::<Employee>(conn)
    }

    /// Insert a new employee record into the database
    // XXX: Might be able to change execute to get_result - http://diesel.rs/guides/all-about-inserts/
    pub fn insert(employee: Employee, conn: &MysqlConnection) -> QueryResult<Employee> {
        diesel::insert_into(employees::table)
            .values(&NewEmployee::from_employee(employee))
            .execute(conn)
            .expect("insert employee");

        employees::dsl::employees.order(employees::dsl::id.desc()).first(conn)
    }

    /// Update an existing employee record in the database
    pub fn update(id: i32, employee: UpdateEmployee, conn: &MysqlConnection) -> QueryResult<Employee> {
        let mut employee_in_db = Employee::get(id, conn).unwrap();

        if let Some(username) = employee.username.clone() {
            employee_in_db.username = username;
        }

        if let Some(first_name) = employee.first_name.clone() {
            employee_in_db.first_name = first_name;
        }

        if let Some(last_name) = employee.last_name.clone() {
            employee_in_db.last_name = last_name;
        }

        if let Some(status_id) = employee.status_id {
            employee_in_db.status_id = status_id;
        }

        if let Some(location_id) = employee.location_id {
            employee_in_db.location_id = location_id;
        }

        diesel::update(employees::table.find(id))
            .set(&employee_in_db)
            .execute(conn)
            .expect("update employee");

        employees::table.find(id).get_result::<Employee>(conn)
    }

    /// Delete an employee record from the database if it exists
    pub fn delete(id: i32, conn: &MysqlConnection) -> QueryResult<usize> {
        diesel::delete(employees::table.find(id))
            .execute(conn)
    }
}
