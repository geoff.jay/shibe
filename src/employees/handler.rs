use rocket::response::{Failure, status};
use rocket_contrib::Json;

use employees::model::{Employee, UpdateEmployee};
use common::{error_status, host, port};
use database::DbConn;

#[get("/", format = "application/json")]
pub fn all(conn: DbConn) -> Result<Json<Vec<Employee>>, Failure> {
    Employee::all(&conn)
        .map(|employee| Json(employee))
        .map_err(|error| error_status(error))
}

#[get("/<id>", format = "application/json")]
fn get(id: i32, conn: DbConn) -> Result<Json<Employee>, Failure> {
    Employee::get(id, &conn)
        .map(|employee| Json(employee))
        .map_err(|error| error_status(error))
}

#[post("/", format = "application/json", data = "<employee>")]
fn post(employee: Json<Employee>, conn: DbConn) -> Result<status::Created<Json<Employee>>, Failure> {
    let insert = Employee { id: None, ..employee.into_inner() };
    Employee::insert(insert, &conn)
        .map(|employee| employee_created(employee))
        .map_err(|error| error_status(error))
}

fn employee_created(employee: Employee) -> status::Created<Json<Employee>> {
    status::Created(
        format!("{host}:{port}/employees/{id}",
                host = host(),
                port = port(),
                id = employee.id.unwrap())
            .to_string(),
        Some(Json(employee)))
}

#[put("/<id>", format = "application/json", data = "<employee>")]
fn put(id: i32, employee: Json<UpdateEmployee>, conn: DbConn) -> Result<Json<Employee>, Failure> {
    Employee::update(id, employee.into_inner(), &conn)
        .map(|rec| Json(rec))
        .map_err(|error| error_status(error))
}

#[delete("/<id>")]
fn delete(id: i32, conn: DbConn) -> Result<status::NoContent, Failure> {
    match Employee::get(id, &conn) {
        Ok(_) => Employee::delete(id, &conn)
            .map(|_| status::NoContent)
            .map_err(|error| error_status(error)),
        Err(error) => Err(error_status(error))
    }
}
