use schema::employees;

//#[derive(Identifiable, Queryable, PartialEq, Debug)]
//#[table_name = "employee_statuses"]
//pub struct EmployeeStatus {
    //pub id: i32,
    //pub status: String,
//}

#[derive(Clone, Debug, Identifiable, Queryable, Associations, AsChangeset, Serialize, Deserialize)]
#[table_name = "employees"]
pub struct Employee {
    pub id: Option<i32>,
    pub username: String,
    pub first_name: String,
    pub last_name: String,
    pub status_id: i32,
    pub location_id: i32,
}

#[derive(Debug, Insertable, Deserialize)]
#[table_name = "employees"]
pub struct NewEmployee {
    pub username: String,
    pub first_name: String,
    pub last_name: String,
    pub status_id: i32,
    pub location_id: i32,
}

#[derive(Debug, Deserialize)]
pub struct UpdateEmployee {
    pub username: Option<String>,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub status_id: Option<i32>,
    pub location_id: Option<i32>,
}

impl NewEmployee {
    pub fn from_employee(employee: Employee) -> NewEmployee {
        NewEmployee {
            username: employee.username,
            first_name: employee.first_name,
            last_name: employee.last_name,
            status_id: employee.status_id,
            location_id: employee.location_id,
        }
    }
}
