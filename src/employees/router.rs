use rocket::Rocket;
use employees;

pub fn deploy(rocket: Rocket, mountpoint: &str) -> Result<Rocket, Rocket> {
    let rocket = rocket.mount(mountpoint,
                              routes![employees::handler::all,
                                      employees::handler::get,
                                      employees::handler::post,
                                      employees::handler::put,
                                      employees::handler::delete]);
    Ok(rocket)
}
