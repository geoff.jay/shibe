# API

All routes given are following the base path, which during development is most
like `http://localhost:8000/v1`.

## Routes

### Default

| Route  | Method | Description |
| ------ | ------ | ----------- |
| /      | GET    |             |

<!-- Future routes - mostly GET
/locations
/locations/<id>
/departments
/departments/<id>
/employees/statuses
/employees/<id>/status
/employees/<id>/location
/employees/<id>/projects
/employees/<id>/departements
/projects/<id>/members
-->

### Employees

| Route           | Method | Description |
| --------------- | ------ | ----------- |
| /employees      | GET    |             |
| /employees/<id> | GET    |             |
| /employees      | POST   |             |
| /employees/<id> | PUT    |             |
| /employees/<id> | DELETE |             |

### Clients

| Route                  | Method | Description |
| ---------------------- | ------ | ----------- |
| /clients               | GET    |             |
| /clients/<id>          | GET    |             |
| /clients/<id>/projects | GET    |             |
| /clients               | POST   |             |
| /clients/<id>          | PUT    |             |
| /clients/<id>          | DELETE |             |

### Projects

| Route          | Method | Description |
| -------------- | ------ | ----------- |
| /projects      | GET    |             |
| /projects/<id> | GET    |             |
| /projects      | POST   |             |
| /projects/<id> | PUT    |             |
| /projects/<id> | DELETE |             |
