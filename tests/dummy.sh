#!/bin/bash

empty() {
  for employee in `curl -s -X GET localhost:8000/employees | jq '.[] | @base64'`; do
    id=`echo $employee | sed 's/"//g' | base64 --decode | jq -r '.id'`
    curl -X DELETE localhost:8000/employees/$id
  done
}

add() {
  curl -X POST localhost:8000/employees \
      -H "Content-Type: application/json" \
      -d @gjohn.json 2>/dev/null \
      | jq -r '.username'
  curl -X POST localhost:8000/employees \
      -H "Content-Type: application/json" \
      -d @jdudl.json 2>/dev/null \
      | jq -r '.username'
}

update() {
  curl -X PUT localhost:8000/employees/11 \
      -H "Content-Type: application/json" \
      -d '{"id": 11, "status_id": 2}'
}

list() {
  curl -X GET localhost:8000/employees 2>/dev/null | jq -C
}

#path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Clear employees"
empty
echo -e "\nAdd employees"
add
#echo -e "\nUpdate employee"
#update
echo -e "\nPrint employees"
list
