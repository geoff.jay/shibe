![Doge!][logo]

# Shibe

I wanted to name something `shibe`.

## Setup

### Web Framework

#### Rocket

Rocket requires rust nightly.

```sh
rustup install nightly
rustup toolchain nightly
```

#### API

See the unfinished doc [here](docs/API.md).

### Database

#### Docker

Using `:latest` doesn't work with the Fedora repository client.

```sh
docker pull mysql/mysql-server:5.7
```

#### Diesel

```sh
cargo install diesel_cli
diesel setup
diesel migration run
```

## Running

This is very much a work in progress and only the development environment has been used so far.

```sh
ID=`docker ps -l -q`
IP=`docker inspect $ID | jq -r '.[0].NetworkSettings.Networks.shibe_default.IPAddress'`
echo "PASSWORD=supergudpw" > .env
echo "DATABASE_URL=mysql://root:$PASSWORD@$IP/shibe_development" >> .env
docker-compose up -d
ROCKET_ENV=dev cargo run -- start
```

[logo]: assets/doge.png
