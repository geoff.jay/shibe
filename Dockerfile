FROM rustlang/rust:nightly

WORKDIR /usr/src/shibe
COPY . .

RUN cargo install

CMD ["shibe start"]
