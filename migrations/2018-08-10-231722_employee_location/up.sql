ALTER TABLE employees ADD location_id INT(11) NOT NULL DEFAULT 1;
ALTER TABLE employees
  ADD CONSTRAINT employees_location_id_fk
  FOREIGN KEY (location_id)
  REFERENCES locations(id);
