CREATE TABLE employee_statuses (
  id INT(11) PRIMARY KEY AUTO_INCREMENT,
  status VARCHAR(16) NOT NULL
) CHARACTER SET utf8mb4;

ALTER TABLE employees ADD status_id INT(11) NOT NULL DEFAULT 2;
ALTER TABLE employees
  ADD CONSTRAINT employees_status_id_fk
  FOREIGN KEY (status_id)
  REFERENCES employee_statuses(id);

-- default data

INSERT INTO employee_statuses (status) VALUES ("In");
INSERT INTO employee_statuses (status) VALUES ("Out");
INSERT INTO employee_statuses (status) VALUES ("Back Later");
