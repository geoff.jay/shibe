CREATE TABLE locations (
  id INT(11) PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(32) NOT NULL,
  city VARCHAR(32) NULL
) CHARACTER SET utf8mb4;

-- default data

INSERT INTO locations (name, city) VALUES ("bby-401", "Burnaby");
INSERT INTO locations (name, city) VALUES ("bby-304", "Burnaby");
INSERT INTO locations (name, city) VALUES ("cgy-100", "Calgary");
INSERT INTO locations (name, city) VALUES ("edm-100", "Edmonton");
